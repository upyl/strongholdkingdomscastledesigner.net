# README #

This is a Net application tool to design castle layouts for the game http://www.strongholdkingdoms.com/. Original Java version is https://code.google.com/p/stronghold-kingdoms-castle-designer/.

Designs are saved as images for easy access, and barcoded to allow the image to be loaded. An alphanumeric version of the barcode can also be exported/imported.

![ScreenClip.png](https://bitbucket.org/repo/xexq8B/images/763605814-ScreenClip.png)

### What is this repository for? ###

* Ported Java code to Net
* 1.0.0.0

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Udgin Pyl (gromkaktus@gmail.com)
* https://code.google.com/p/stronghold-kingdoms-castle-designer/

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.