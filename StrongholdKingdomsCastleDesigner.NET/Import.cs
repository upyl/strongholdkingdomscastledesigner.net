﻿using System;
using System.Windows.Forms;

namespace StrongholdKingdomsCastleDesigner.NET
{
    public partial class Import : Form
    {
        public event EventHandler<string> ImportedData;

        public Import()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ImportedData != null)
            {
                ImportedData(this, this.richTextBox1.Text);
            }
            this.Close();
        }
    }
}