﻿using System.Windows.Forms;

namespace StrongholdKingdomsCastleDesigner.NET
{
    public partial class Export : Form
    {
        public Export()
        {
            InitializeComponent();
        }

        public void ChangeText(string text)
        {
            this.richTextBox1.Text = text;
        }

        private void coptyToClipboard_Click(object sender, System.EventArgs e)
        {
            Clipboard.SetText(this.richTextBox1.Text);
        }
    }
}