﻿using StrongholdKingdomsCastleDesigner.NET.src;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace StrongholdKingdomsCastleDesigner.NET
{
    public partial class Editor : Form
    {
        private const string FILE_EXTENSION = "png";

        private FileInfo currentFile;

        public Editor()
        {
            InitializeComponent();

            this.landPanel = new LandPanel();
            landPanel.AllowDrop = true;
            landPanel.AutoSize = true;
            //this.Controls.Add(landPanel);

            this.Resize += new EventHandler((obj, e) => landPanel.getLandGrid().Refresh());
            this.splitContainer1.Panel1.Scroll += new ScrollEventHandler((obj, e) => landPanel.getLandGrid().Refresh());
            this.splitContainer1.Panel1.MouseWheel += new MouseEventHandler((obj, e) => landPanel.getLandGrid().Refresh());

            var errorPanel = new Panel();

            landPanel.getLandGrid().addDesignListener(new DesignListener()
            {
                designChanged = () =>
                {
                    var designError = string.Join(", ",
                    landPanel.getLandGrid().getCastle().getDesignErrors().ToArray());

                    var designErrorLabel = new Label();
                    designErrorLabel.Text = designError;
                    designErrorLabel.AutoSize = true;
                    errorPanel.Controls.Add(designErrorLabel);
                }
            });

            BuildingsPanel buildingsPanel = new BuildingsPanel();
            buildingsPanel.setCastle(landPanel.getLandGrid().getCastle());
            buildingsPanel.SelectedBuildingChanged += (sender, type) =>
            {
                landPanel.getLandGrid().setSelectedBuilding(type);
            };
            landPanel.getLandGrid().addDesignListener(new DesignListener()
            {
                designChanged = buildingsPanel.designChanged
            });

            var tipsPanel = new GroupBox();
            tipsPanel.Text = "Tips!";
            tipsPanel.AutoSize = true;
            tipsPanel.Controls.Add(new Label
            {
                Text = "Drag the mouse for faster placement of buildings",
                Location = new System.Drawing.Point(6, 16),
                AutoSize = true
            });
            tipsPanel.Controls.Add(new Label
            {
                Text = "Use the right mouse button to delete",
                Location = new System.Drawing.Point(6, 48),
                AutoSize = true
            });

            this.splitContainer1.Panel1.Controls.Add(landPanel);
            this.flowLayoutPanel1.Controls.Add(buildingsPanel);
            this.flowLayoutPanel1.Controls.Add(errorPanel);
            this.flowLayoutPanel1.Controls.Add(tipsPanel);

            this.Refresh();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            currentFile = null;

            landPanel.getLandGrid().clearData();
        }

        private void createToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = openFileDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                var file = openFileDialog1.FileName;
                try
                {
                    String importString = null;
                    if (file.EndsWith("." + FILE_EXTENSION))
                    {
                        var bufferedImage = Bitmap.FromFile(file) as Bitmap;
                        importString = Barcode.extractBarcode(bufferedImage);
                    }
                    else
                    {
                        using (var reader = File.OpenText(file))
                        {
                            importString = reader.ReadLine();
                        }
                    }
                    importData(importString);
                    currentFile = new FileInfo(file);
                }
                catch (IOException ex)
                {
                    //Logger.getLogger(Editor.class.getName()).log(Level.SEVERE, null, ex);
                    //errorMessage = ex.getLocalizedMessage();
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var exportString = generateExportString();
            var exportWindow = new StrongholdKingdomsCastleDesigner.NET.Export();
            exportWindow.ChangeText(exportString);

            exportWindow.ShowDialog(this);
        }

        private String generateExportString()
        {
            return landPanel.getLandGrid().getCastle().getGridDataExport();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var aboutBox1 = new StrongholdKingdomsCastleDesigner.NET.AboutBox1();
            aboutBox1.ShowDialog(this);
        }

        private void importData(String text)
        {
            if (text == null || text.Length == 0) return;

            landPanel.getLandGrid().importData(text);
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var import = new StrongholdKingdomsCastleDesigner.NET.Import();
            import.ImportedData += (send, value) =>
            {
                this.importData(value);
            };
            import.ShowDialog(this);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showSaveDialog();
        }

        private void saveFile(FileInfo file)
        {
            try
            {
                Bitmap bufferedImage = landPanel.getDesignImage();
                Barcode.embedBarcode(bufferedImage, generateExportString());

                bufferedImage.Save(file.FullName);
                currentFile = file;
            }
            catch (IOException ex)
            {
                //Logger.getLogger(Editor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentFile == null || !currentFile.Name.EndsWith('.' + FILE_EXTENSION))
            {
                showSaveDialog();
            }
            else
            {
                saveFile(currentFile);
            }
        }

        private void showSaveDialog()
        {
            var result = saveFileDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                saveFile(new FileInfo(saveFileDialog1.FileName));
            }
        }
    }
}