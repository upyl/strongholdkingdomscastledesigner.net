﻿namespace StrongholdKingdomsCastleDesigner.NET.src
{
    public class TileBuilding
    {
        private readonly BuildingType buildingType;
        private int buildingId;

        public TileBuilding(BuildingType buildingType, int buildingId)
        {
            this.buildingId = buildingId;
            this.buildingType = buildingType;
        }

        public int getBuildingId()
        {
            return buildingId;
        }

        public BuildingType getBuildingType()
        {
            return buildingType;
        }

        public void setBuildingId(int id)
        {
            this.buildingId = id;
        }
    }
}