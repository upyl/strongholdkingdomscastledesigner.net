﻿namespace StrongholdKingdomsCastleDesigner.NET.src
{
    public enum BuildingResource
    {
        STONE, WOOD, IRON, GOLD
    }
}