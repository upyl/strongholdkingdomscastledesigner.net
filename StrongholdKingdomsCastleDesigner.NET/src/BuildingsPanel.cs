﻿using System;
using System.Text;
using System.Windows.Forms;
using Dimension = System.Drawing.Size;

namespace StrongholdKingdomsCastleDesigner.NET.src
{
    public class BuildingsPanel : FlowLayoutPanel
    {
        public const String SELECTED_BUILDING = "SelectedBuilding";
        private static Dimension buttonDimension = new Dimension(125, 65);
        private static int[] constructionBonuses = new int[] { 0, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60 };
        private Label ballistaTowersLabel;
        private Castle castle;
        private TrackBar constructionSpeed;
        private Label goldLabel;
        private Label guardHousesLabel;
        private Label ironLabel;
        private Label moatsLabel;
        private Label stoneLabel;
        private Label timeLabel;
        private Label turretsLabel;
        private Label woodLabel;

        public BuildingsPanel()
        {
            Button woodenWallButton = createButton("Wooden Wall", BuildingType.WOODEN_WALL);
            Button woodenTowerButton = createButton("Wooden Tower", BuildingType.WOODEN_TOWER);
            Button woodenGatehouseButton = createButton("Wooden Gatehouse", BuildingType.WOODEN_GATEHOUSE);

            Button stoneWallButton = createButton("Stone Wall", BuildingType.STONE_WALL);
            Button stoneGatehouseButton = createButton("Stone Gatehouse", BuildingType.STONE_GATEHOUSE);
            Button lookoutTowerButton = createButton("Lookout Tower (2x2)", BuildingType.LOOKOUT_TOWER);
            Button smallTowerButton = createButton("Small Tower (3x3)", BuildingType.SMALL_TOWER);
            Button largeTowerButton = createButton("Large Tower (4x4)", BuildingType.LARGE_TOWER);
            Button greatTowerButton = createButton("Great Tower (5x5)", BuildingType.GREAT_TOWER);

            Button guardHouseButton = createButton("Guard House", BuildingType.GUARD_HOUSE);
            Button ballistaTowerButton = createButton("Ballista Tower", BuildingType.BALLISTA_TOWER);
            Button turretButton = createButton("Turret", BuildingType.TURRET);
            Button smelterButton = createButton("Smelter", BuildingType.SMELTER);
            Button moatButton = createButton("Moat", BuildingType.MOAT);
            Button killingPitButton = createButton("Killing Pit", BuildingType.KILLING_PIT);
            this.AutoSize = true;
            //this.MaximumSize = new Dimension(300, 875);
            //this.MinimumSize = new Dimension(300, 875);
            //this.Size = new Dimension(300, 875);

            var buttonsPanel = new TableLayoutPanel
            {
                RowCount = 8,
                ColumnCount = 2
            };

            buttonsPanel.Controls.Add(stoneWallButton, 0, 0);
            buttonsPanel.Controls.Add(woodenWallButton, 1, 0);
            buttonsPanel.Controls.Add(stoneGatehouseButton, 0, 1);
            buttonsPanel.Controls.Add(woodenGatehouseButton, 1, 1);
            buttonsPanel.Controls.Add(guardHouseButton, 0, 2);
            buttonsPanel.Controls.Add(woodenTowerButton, 1, 2);
            buttonsPanel.Controls.Add(lookoutTowerButton, 0, 3);
            buttonsPanel.Controls.Add(smallTowerButton, 1, 3);
            buttonsPanel.Controls.Add(largeTowerButton, 0, 4);
            buttonsPanel.Controls.Add(greatTowerButton, 1, 4);
            buttonsPanel.Controls.Add(smelterButton, 0, 5);
            buttonsPanel.Controls.Add(ballistaTowerButton, 1, 5);
            buttonsPanel.Controls.Add(turretButton, 0, 6);
            buttonsPanel.Controls.Add(moatButton, 1, 6);
            buttonsPanel.Controls.Add(killingPitButton, 0, 7);
            buttonsPanel.Dock = DockStyle.Top;
            buttonsPanel.AutoSize = true;
            this.Controls.Add(buttonsPanel);

            var quantitiesPanel = createQuantitiesPanel();
            this.Controls.Add(quantitiesPanel);

            var resourcesPanel = createResourcesPanel();
            this.Controls.Add(resourcesPanel);

            var timePanel = createTimePanel();
            this.Controls.Add(timePanel);
        }

        public event EventHandler<BuildingType> SelectedBuildingChanged;

        public void designChanged()
        {
            updateBuildingQuantities();
            updateResources();
            updateTotalBuildingTime();
        }

        public void setCastle(Castle castle)
        {
            this.castle = castle;
            updateBuildingQuantities();
        }

        private Button createButton(String buttonText, BuildingType buildingType)
        {
            Button button = new Button();
            button.Text = buttonText;
            if (buildingType.Image != null)
            {
                button.Image = buildingType.Image;
            }
            else
            {
                button.BackColor = buildingType.Color;
            }

            button.MinimumSize = buttonDimension;
            button.Size = buttonDimension;
            button.TextImageRelation = TextImageRelation.ImageBeforeText;
            button.Click += (obj, sender) =>
            {
                this.SelectedBuildingChanged(this, buildingType);
            };
            return button;
        }

        private Panel createQuantitiesPanel()
        {
            var quantitiesPanel = new TableLayoutPanel();
            quantitiesPanel.RowCount = 4;
            quantitiesPanel.ColumnCount = 2;
            quantitiesPanel.AutoSize = true;

            Label moatsTitleLabel = new Label { Text = "Moats" };
            moatsLabel = new Label { Text = "-/-" };
            var ballistaTowersTitleLabel = new Label { Text = "Ballista Towers" };
            ballistaTowersLabel = new Label { Text = "-/-" };
            var turretsTitleLabel = new Label { Text = "Turrets" };
            turretsLabel = new Label { Text = "-/-" };
            var guardHousesTitleLabel = new Label { Text = "Guard Houses" };
            guardHousesLabel = new Label { Text = "-/-" };

            quantitiesPanel.Controls.Add(guardHousesTitleLabel, 0, 0);
            quantitiesPanel.Controls.Add(guardHousesLabel, 1, 0);
            quantitiesPanel.Controls.Add(ballistaTowersTitleLabel, 0, 1);
            quantitiesPanel.Controls.Add(ballistaTowersLabel, 1, 1);
            quantitiesPanel.Controls.Add(turretsTitleLabel, 0, 2);
            quantitiesPanel.Controls.Add(turretsLabel, 1, 2);
            quantitiesPanel.Controls.Add(moatsTitleLabel, 0, 3);
            quantitiesPanel.Controls.Add(moatsLabel, 1, 3);

            return quantitiesPanel;
        }

        private TableLayoutPanel createResourcesPanel()
        {
            TableLayoutPanel resourcesPanel = new TableLayoutPanel();
            resourcesPanel.RowCount = 4;
            resourcesPanel.ColumnCount = 2;
            Label stoneTitleLabel = new Label { Text = "Stone" };
            stoneLabel = new Label { Text = "0" };
            Label woodTitleLabel = new Label { Text = "Wood" };
            woodLabel = new Label { Text = "0" };
            Label ironTitleLabel = new Label { Text = "Iron" };
            ironLabel = new Label { Text = "0" };
            Label goldTitleLabel = new Label { Text = "Gold" };
            goldLabel = new Label { Text = "0" };
            resourcesPanel.Controls.Add(stoneTitleLabel, 0, 0);
            resourcesPanel.Controls.Add(stoneLabel, 1, 0);
            resourcesPanel.Controls.Add(woodTitleLabel, 0, 1);
            resourcesPanel.Controls.Add(woodLabel, 1, 1);
            resourcesPanel.Controls.Add(ironTitleLabel, 0, 2);
            resourcesPanel.Controls.Add(ironLabel, 1, 2);
            resourcesPanel.Controls.Add(goldTitleLabel, 0, 3);
            resourcesPanel.Controls.Add(goldLabel, 1, 3);

            return resourcesPanel;
        }

        private Panel createTimePanel()
        {
            timeLabel = new Label { Text = "0s" };

            constructionSpeed = new TrackBar();
            constructionSpeed.Orientation = Orientation.Horizontal;
            constructionSpeed.Maximum = 10;
            constructionSpeed.Minimum = 0;
            constructionSpeed.TickFrequency = 1;

            constructionSpeed.ValueChanged += (sender, e) =>
            {
                updateTotalBuildingTime();
            };

            var timePanel = new TableLayoutPanel()
            {
                RowCount = 1,
                ColumnCount = 2
            };

            timePanel.Controls.Add(timeLabel, 0, 0);
            timePanel.Controls.Add(constructionSpeed, 1, 0);

            return timePanel;
        }

        private String formatTime(int totalBuildingTime)
        {
            int time = totalBuildingTime;
            StringBuilder s = new StringBuilder();

            int seconds = time % 60;
            int minutes = (time / 60) % 60;
            int hours = (time / 3600) % 24;
            int days = (time / 86400);

            if (days > 0)
            {
                s.Append(days);
                s.Append("d ");
            }
            if (days > 0 || hours > 0)
            {
                s.Append(hours);
                s.Append("h ");
            }
            if (days > 0 || hours > 0 || minutes > 0)
            {
                s.Append(minutes);
                s.Append("m ");
            }
            s.Append(seconds);
            s.Append('s');

            return s.ToString();
        }

        private String getBuildingQuantities(BuildingType buildingType)
        {
            int numberOfBuildings = 0;
            int maxNumberOfBuildings = 0;
            if (castle != null)
            {
                numberOfBuildings = castle.getNumberOfBuildings(buildingType);
                maxNumberOfBuildings = castle.getMaximumNumberOfBuildings(buildingType);
            }

            bool exceeded = numberOfBuildings > maxNumberOfBuildings;
            bool maximum = numberOfBuildings == maxNumberOfBuildings;

            StringBuilder s = new StringBuilder();
            if (exceeded | maximum) s.Append("");

            if (exceeded) s.Append("Exceed: ");
            else if (maximum) s.Append("Maximum: ");
            s.Append(numberOfBuildings);
            s.Append('/');
            s.Append(maxNumberOfBuildings);
            if (exceeded | maximum) s.Append("");

            return s.ToString();
        }

        private String getBuildingResources(BuildingResource buildingResource)
        {
            if (castle != null)
            {
                return castle.getTotalResource(buildingResource).ToString();
            }
            else return "0";
        }

        private void updateBuildingQuantities()
        {
            moatsLabel.Text = getBuildingQuantities(BuildingType.MOAT);
            ballistaTowersLabel.Text = getBuildingQuantities(BuildingType.BALLISTA_TOWER);
            turretsLabel.Text = getBuildingQuantities(BuildingType.TURRET);
            guardHousesLabel.Text = getBuildingQuantities(BuildingType.GUARD_HOUSE);
        }

        private void updateResources()
        {
            stoneLabel.Text = getBuildingResources(BuildingResource.STONE);
            woodLabel.Text = getBuildingResources(BuildingResource.WOOD);
            ironLabel.Text = getBuildingResources(BuildingResource.IRON);
            goldLabel.Text = getBuildingResources(BuildingResource.GOLD);
        }

        private void updateTotalBuildingTime()
        {
            if (castle != null)
            {
                int researchDiscount = constructionBonuses[constructionSpeed.Value];
                int buildingTime = (int)(castle.getTotalBuildingTime() * (1 - (((double)researchDiscount) / 100)));

                timeLabel.Text = formatTime(buildingTime);
            }
            else timeLabel.Text = "0s";
        }
    }
}