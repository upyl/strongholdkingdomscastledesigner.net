﻿using System.Drawing;
using System.Windows.Forms;

namespace StrongholdKingdomsCastleDesigner.NET.src
{
    public class LandPanel : Panel
    {
        private LandGrid landGrid;

        public LandPanel()
        {
            landGrid = new LandGrid();

            this.Controls.Add(landGrid);
        }

        public Bitmap getDesignImage()
        {
            Graphics gr1 = landGrid.CreateGraphics();
            Bitmap bmp1 = new Bitmap(landGrid.Width, landGrid.Height);
            landGrid.DrawToBitmap(bmp1, new Rectangle(0, 0, landGrid.Width, landGrid.Height));
            return bmp1;
        }

        public LandGrid getLandGrid()
        {
            return landGrid;
        }
    }
}