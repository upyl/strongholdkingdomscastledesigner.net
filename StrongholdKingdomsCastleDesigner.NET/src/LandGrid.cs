﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Dimension = System.Drawing.Size;

namespace StrongholdKingdomsCastleDesigner.NET.src
{
    public class LandGrid : PictureBox
    {
        public const int gridOffsetX = 14;
        public const int gridOffsetY = 14;
        public const int tileWidth = 14;
        private static Color GRASS = Color.FromArgb(0, 125, 0);
        private MouseButtons button = MouseButtons.None;
        private Castle castle = new Castle();
        private String coordinates = "[0, 0]";
        private List<DesignListener> designListeners = new List<DesignListener>();
        private int[] mouseCoords = { 0, 0 };
        private BuildingType selectedBuilding = BuildingType.STONE_WALL;

        public LandGrid()
        {
            resetGridData();

            this.Paint += new PaintEventHandler(paintComponent);
            this.MouseMove += new MouseEventHandler(mouseMoved);
            this.MouseClick += new MouseEventHandler(mouseClicked);

            this.MinimumSize = new Size(
            tileWidth * Castle.CASTLE_BOUNDRY_LENGTH + gridOffsetX + 1,
            tileWidth * Castle.CASTLE_BOUNDRY_LENGTH + gridOffsetY + 1);
        }

        public void addDesignListener(DesignListener designListener)
        {
            designListeners.Add(designListener);
        }

        public void clearData()
        {
            resetGridData();
            this.Refresh();
        }

        public Castle getCastle()
        {
            return castle;
        }

        public BuildingType getSelectedBuilding()
        {
            return selectedBuilding;
        }

        public void importData(String text)
        {
            castle.importData(text);
            notifyDesignListeners();

            this.Refresh();
        }

        public void removeDesignListener(DesignListener designListener)
        {
            designListeners.Remove(designListener);
        }

        public void setSelectedBuilding(BuildingType building)
        {
            if (building == null) throw new NoNullAllowedException("Invalid argument (null) to setSelectedBuilding");
            this.selectedBuilding = building;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            paintComponent(this, pe);
        }

        private void changeGridData(MouseEventArgs e)
        {
            mouseCoords = getCoords(e.X, e.Y);

            //If left click
            if (button == MouseButtons.Left)
            {
                if (isValidCoords(mouseCoords))
                {
                    Dimension dimension = selectedBuilding.Dimension;
                    int[] hotspot = selectedBuilding.getHotspot();

                    List<Point> buildingCoords = new List<Point>();

                    for (int i = mouseCoords[0] - hotspot[0]; i < mouseCoords[0] - hotspot[0] + dimension.Width; i++)
                    {
                        for (int j = mouseCoords[1] - hotspot[1]; j < mouseCoords[1] - hotspot[1] + dimension.Height; j++)
                        {
                            buildingCoords.Add(new Point(i, j));
                        }
                    }
                    castle.addBuilding(buildingCoords, selectedBuilding);

                    this.Refresh();
                    notifyDesignListeners();
                }
            }
            else if (button == MouseButtons.Right) //else if right click
            {
                if (fitsInGrid(mouseCoords, new Dimension(1, 1), new int[] { 0, 0 }))
                {
                    TileBuilding building = castle.getGridData(mouseCoords[0], mouseCoords[1]);
                    if (building != null && building.getBuildingType() != BuildingType.KEEP)
                    {
                        castle.removeBuilding(building);
                    }
                    this.Refresh();
                    notifyDesignListeners();
                }
            }
        }

        private void drawGrid(Panel component, PaintEventArgs e)
        {
            var g = e.Graphics;
            var penBlack = new Pen(Color.Black);
            for (int i = 0; i < Castle.CASTLE_BOUNDRY_LENGTH + 1; i++)
            {
                g.DrawLine(penBlack, i * tileWidth + gridOffsetX, gridOffsetY, i * tileWidth + gridOffsetX, tileWidth * Castle.CASTLE_BOUNDRY_LENGTH + gridOffsetY);
                g.DrawLine(penBlack, gridOffsetX, i * tileWidth + gridOffsetY, tileWidth * Castle.CASTLE_BOUNDRY_LENGTH + gridOffsetX, i * tileWidth + gridOffsetY);
            }
        }

        private bool fitsInGrid(int[] coords, Dimension dimension, int[] hotspot)
        {
            return (coords[0] - hotspot[0] >= 0 &&
                coords[0] - hotspot[0] <= Castle.CASTLE_BOUNDRY_LENGTH - dimension.Width &&
                coords[1] - hotspot[1] >= 0 &&
                coords[1] - hotspot[1] <= Castle.CASTLE_BOUNDRY_LENGTH - dimension.Height);
        }

        private int[] getCoords(int ex, int ey)
        {
            return new int[] {(int)Math.Floor(((double)(ex - gridOffsetX)) / tileWidth),
                (int)Math.Floor(((double)(ey - gridOffsetY)) / tileWidth)};
        }

        private bool isBuildable(int[] coords, Dimension dimension, int[] hotspot)
        {
            for (int i = coords[0] - hotspot[0]; i < coords[0] - hotspot[0] + dimension.Width; i++)
            {
                for (int j = coords[1] - hotspot[1]; j < coords[1] - hotspot[1] + dimension.Height; j++)
                {
                    TileBuilding tileBuilding = castle.getGridData(i, j);

                    if (tileBuilding != null &&
                        tileBuilding.getBuildingType() != BuildingType.WOODEN_WALL &&
                        tileBuilding.getBuildingType() != BuildingType.STONE_WALL)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        private bool isGapSatisfied(int[] coords, Dimension dimension, int[] hotspot)
        {
            for (int i = coords[0] - hotspot[0] - 1; i < coords[0] - hotspot[0] + dimension.Width + 1; i++)
            {
                for (int j = coords[1] - hotspot[1] - 1; j < coords[1] - hotspot[1] + dimension.Height + 1; j++)
                {
                    if (i >= 0 && i < Castle.CASTLE_BOUNDRY_LENGTH && j >= 0 && j < Castle.CASTLE_BOUNDRY_LENGTH)
                    {
                        TileBuilding tileBuilding = castle.getGridData(i, j);

                        if (tileBuilding != null && tileBuilding.getBuildingType().GapRequired) return false;
                    }
                }
            }
            return true;
        }

        private bool isValidCoords(int[] coords)
        {
            Dimension dimension = selectedBuilding.Dimension;
            int[] hotspot = selectedBuilding.getHotspot();

            if (fitsInGrid(coords, dimension, hotspot) &&
                isBuildable(coords, dimension, hotspot))
            {
                if (selectedBuilding.GapRequired)
                {
                    return isGapSatisfied(coords, dimension, hotspot);
                }
                else return true;
            }
            else return false;
        }

        private void mouseClicked(object sender, MouseEventArgs e)
        {
            button = e.Button;
            changeGridData(e);
        }

        private void mouseDown(object sender, MouseEventArgs e)
        {
            button = e.Button;
            changeGridData(e);
        }

        private void mouseMoved(object sender, MouseEventArgs e)
        {
            mouseCoords = getCoords(e.X, e.Y);

            coordinates = "[" + mouseCoords[0] + ", " + mouseCoords[1] + "]";
            this.Invalidate();
        }

        private void notifyDesignListeners()
        {
            foreach (DesignListener designListener in designListeners)
            {
                designListener.designChanged();
            }
        }

        private void paintComponent(object sender, PaintEventArgs e)
        {
            var p = sender as PictureBox;
            var g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
            g.DrawString(coordinates, DefaultFont, Brushes.Black, 0, 0);
            g.TranslateClip(gridOffsetX, gridOffsetY);

            //Draw any plain colours, e.g. grass & keep
            for (int i = 0; i < Castle.CASTLE_BOUNDRY_LENGTH; i++)
            {
                for (int j = 0; j < Castle.CASTLE_BOUNDRY_LENGTH; j++)
                {
                    TileBuilding tileBuilding = castle.getGridData(i, j);

                    SolidBrush brush;
                    if (tileBuilding != null && tileBuilding.getBuildingType().Image == null)
                    {
                        //This is unlikely to be called now since everything has an image
                        brush = new SolidBrush(tileBuilding.getBuildingType().Color);
                    }
                    else
                    {
                        brush = new SolidBrush(GRASS);
                    }

                    g.FillRectangle(brush, i * tileWidth + gridOffsetX, j * tileWidth + gridOffsetY, tileWidth, tileWidth);
                }
            }

            //Draw the grid
            var penBlack = new Pen(Color.Black);
            for (int i = 0; i < Castle.CASTLE_BOUNDRY_LENGTH + 1; i++)
            {
                g.DrawLine(penBlack, i * tileWidth + gridOffsetX, gridOffsetY, i * tileWidth + gridOffsetX, tileWidth * Castle.CASTLE_BOUNDRY_LENGTH + gridOffsetY);
                g.DrawLine(penBlack, gridOffsetX, i * tileWidth + gridOffsetY, tileWidth * Castle.CASTLE_BOUNDRY_LENGTH + gridOffsetX, i * tileWidth + gridOffsetY);
            }

            //Draw the buildings
            List<int> ids = new List<int>();
            for (int i = 0; i < Castle.CASTLE_BOUNDRY_LENGTH; i++)
            {
                for (int j = 0; j < Castle.CASTLE_BOUNDRY_LENGTH; j++)
                {
                    TileBuilding tileBuilding = castle.getGridData(i, j);
                    if (tileBuilding != null)
                    {
                        if ((tileBuilding.getBuildingType().Image != null) &&
                            !ids.Contains(tileBuilding.getBuildingId()))
                        {
                            ids.Add(tileBuilding.getBuildingId());
                            g.DrawImage(tileBuilding.getBuildingType().Image, i * tileWidth + 1 + gridOffsetX, j * tileWidth + 1 + gridOffsetY, tileBuilding.getBuildingType().Image.Width,
                                tileBuilding.getBuildingType().Image.Height);
                        }
                    }
                }
            }

            //Draw the mouse-overlay
            Image overlay;
            if (isValidCoords(mouseCoords)) overlay = selectedBuilding.ValidOverlay;
            else
                overlay = selectedBuilding.InvalidOverlay;

            int[] hotspot = selectedBuilding.getHotspot();

            g.DrawImage(overlay, (mouseCoords[0] - hotspot[0]) * tileWidth + 1 + gridOffsetX, (mouseCoords[1] - hotspot[1]) * tileWidth + 1 + gridOffsetY, overlay.Width - 1, overlay.Height - 1);
        }

        private void resetGridData()
        {
            castle.resetGridData();
            notifyDesignListeners();
        }
    }
}