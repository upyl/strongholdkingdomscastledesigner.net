﻿using System;
using System.Data;
using System.Drawing;
using System.Text;

namespace StrongholdKingdomsCastleDesigner.NET.src
{
    public class Barcode
    {
        private const uint border = 0xff660066;
        private const uint END_CODE = 255 << 4;
        private const int endingY = 15;
        private const int MINIMUM_HEIGHT = endingY + 1;
        private const int MINIMUM_WIDTH = startingX + 600;
        private const uint OPAQUE_MASK = 0xff000000;
        private const uint SEPARATOR_CODE = 254 << 4;

        //Must be > border width
        private const int startingX = 50;

        //Must be > border width
        private const int startingY = 2;

        public static void embedBarcode(Bitmap bufferedImage, String barcode)
        {
            if (bufferedImage == null) throw new NoNullAllowedException("Null image");
            if (bufferedImage.Width < MINIMUM_WIDTH || bufferedImage.Height <= MINIMUM_HEIGHT)
            {
                throw new ArgumentException("Invalid image size");
            }

            int x = startingX;
            int y = startingY;
            foreach (char c in barcode.ToCharArray())
            {
                uint code;

                if (c == 'Z') code = SEPARATOR_CODE;
                else
                {
                    /*
                     * Yes I know we're going in circles a bit here, converting into alphanumeric
                     * and now back again to a number. It's not ideal, but I didn't forsee
                     * this in the design, oops!
                     */
                    code = Converter.alphaNumericToInt(c) << 4;
                }

                bufferedImage.SetPixel(x, y, ColorExtension.UIntToColor(OPAQUE_MASK | code));

                y++;
                if (y > endingY)
                {
                    y = startingY;
                    x++;
                }
            }
            bufferedImage.SetPixel(x, y, ColorExtension.UIntToColor(OPAQUE_MASK | END_CODE));

            //Fill in the final column to make it pretty
            for (int j = y + 1; j <= endingY; j++) bufferedImage.SetPixel(x, j, ColorExtension.UIntToColor(0xff000000));

            addBorder(bufferedImage, x);
        }

        public static String extractBarcode(Bitmap bufferedImage)
        {
            if (bufferedImage.Width < MINIMUM_WIDTH || bufferedImage.Height < MINIMUM_HEIGHT)
            {
                throw new ArgumentException("Incorrect image size");
            }

            try
            {
                StringBuilder s = new StringBuilder();

                int x = startingX;
                int y = startingY;
                uint rgb = ColorExtension.ColorToUInt(bufferedImage.GetPixel(x, y));

                //Some codes spill into the green bits because we're shifting by 4, so we
                //need to mask with 0x00000fff
                while ((rgb & 0xfff) != END_CODE)
                {
                    s.Append(decodeRGB(rgb));
                    y++;
                    if (y > endingY)
                    {
                        y = startingY;
                        x++;
                    }
                    rgb = ColorExtension.ColorToUInt(bufferedImage.GetPixel(x, y));
                }
                return s.ToString();
            }
            catch (ArgumentException e)
            {
                throw;
            }
        }

        private static void addBorder(Bitmap bufferedImage, int endX)
        {
            var t = ColorExtension.UIntToColor(border);
            for (int x = startingX - 1; x <= endX + 1; x++)
            {
                bufferedImage.SetPixel(x, startingY - 1, t);
                bufferedImage.SetPixel(x, endingY + 1, t);
            }
            for (int y = startingY; y <= endingY; y++)
            {
                bufferedImage.SetPixel(startingX - 1, y, t);
                bufferedImage.SetPixel(endX + 1, y, t);
            }
        }

        private static char decodeRGB(uint rgb)
        {
            uint code = rgb & 0xfff;

            if (code == SEPARATOR_CODE) return 'Z';
            else return Converter.intToAlphaNumeric(code >> 4);
        }
    }
}