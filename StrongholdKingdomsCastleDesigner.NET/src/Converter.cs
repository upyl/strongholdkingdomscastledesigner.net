﻿using System;

namespace StrongholdKingdomsCastleDesigner.NET.src
{
    internal static class Converter
    {
        public const char seperator = 'Z';

        public static uint alphaNumericToInt(char alphaNumeric)
        {
            if (alphaNumeric >= 'A' && alphaNumeric < 'Z') return Convert.ToUInt32(alphaNumeric - 'A' + 36);
            else if (alphaNumeric >= 'a' && alphaNumeric <= 'z') return Convert.ToUInt32(alphaNumeric - 'a' + 10);
            else if (alphaNumeric >= '0' && alphaNumeric <= '9') return Convert.ToUInt32(alphaNumeric - '0');
            else throw new ArgumentOutOfRangeException("Unexpected character in import: " + alphaNumeric);
        }

        public static char intToAlphaNumeric(uint integer)
        {
            if (integer < 0 || integer > 60) throw new ArgumentOutOfRangeException("Invalid number: " + integer);

            if (integer < 10) return (char)(integer + '0');
            else if (integer < 36) return (char)(integer - 10 + 'a');
            else return (char)(integer - 36 + 'A');
        }
    }
}