﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;

namespace StrongholdKingdomsCastleDesigner.NET.src
{
    public class Castle
    {
        public const int CASTLE_BOUNDRY_LENGTH = 52;
        public const int exportVersionId = 2;﻿

        private static int lastIdUsed = 0;
        private IDictionary<BuildingType, int> buildingQuantities = new Dictionary<BuildingType, int>();
        private IDictionary<BuildingResource, int> buildingResources = new Dictionary<BuildingResource, int>();
        private List<string> designErrors = new List<string>();
        private TileBuilding[][] gridData = new TileBuilding[CASTLE_BOUNDRY_LENGTH][];
        private IDictionary<BuildingType, int> maxBuildings;
        private int totalBuildingTime = 0;

        public Castle()
        {
            maxBuildings = new Dictionary<BuildingType, int>();
            setMaxBuildings();

            for (var i = 0; i < gridData.Length; i++)
            {
                gridData[i] = new TileBuilding[CASTLE_BOUNDRY_LENGTH];
            }

            //Call reset here to set the Keep in the centre
            resetGridData();
        }

        public void addBuilding(IList<Point> buildingCoords, BuildingType buildingType)
        {
            int id = getNewId();

            foreach (Point p in buildingCoords)
            {
                gridData[p.X][p.Y] = new TileBuilding(buildingType, id);
            }
            updateDesignStats();
        }

        public List<String> getDesignErrors()
        {
            return designErrors;
        }

        public TileBuilding getGridData(int x, int y)
        {
            return gridData[x][y];
        }

        public String getGridDataExport()
        {
            StringBuilder woodenWalls = new StringBuilder();
            StringBuilder stoneWalls = new StringBuilder();
            StringBuilder moats = new StringBuilder();
            StringBuilder killingPits = new StringBuilder();
            StringBuilder structures = new StringBuilder();

            IList<int> ids = new List<int>();

            for (uint i = 0; i < gridData.Length; i++)
            {
                for (uint j = 0; j < gridData[i].Length; j++)
                {
                    TileBuilding building = gridData[i][j];

                    if (building != null)
                    {
                        if (building.getBuildingType() == BuildingType.WOODEN_WALL)
                        {
                            woodenWalls.Append(Converter.intToAlphaNumeric(i));
                            woodenWalls.Append(Converter.intToAlphaNumeric(j));
                        }
                        else if (building.getBuildingType() == BuildingType.STONE_WALL)
                        {
                            stoneWalls.Append(Converter.intToAlphaNumeric(i));
                            stoneWalls.Append(Converter.intToAlphaNumeric(j));
                        }
                        else if (building.getBuildingType() == BuildingType.MOAT)
                        {
                            moats.Append(Converter.intToAlphaNumeric(i));
                            moats.Append(Converter.intToAlphaNumeric(j));
                        }
                        else if (building.getBuildingType() == BuildingType.KILLING_PIT)
                        {
                            killingPits.Append(Converter.intToAlphaNumeric(i));
                            killingPits.Append(Converter.intToAlphaNumeric(j));
                        }
                        else
                        {
                            if (!ids.Contains(building.getBuildingId()))
                            {
                                ids.Add(building.getBuildingId());
                                structures.Append(Converter.intToAlphaNumeric(building.getBuildingType().Order));
                                structures.Append(Converter.intToAlphaNumeric(i));
                                structures.Append(Converter.intToAlphaNumeric(j));
                            }
                        }
                    }
                }
            }

            StringBuilder exportStringBuffer = new StringBuilder();
            exportStringBuffer.Append(exportVersionId);

            return exportStringBuffer.Append(woodenWalls)
                        .Append(Converter.seperator)
                        .Append(stoneWalls)
                        .Append(Converter.seperator)
                        .Append(structures)
                        .Append(Converter.seperator)
                        .Append(moats)
                        .Append(Converter.seperator)
                        .Append(killingPits).ToString();
        }

        public int getMaximumNumberOfBuildings(BuildingType buildingType)
        {
            int max = maxBuildings[buildingType];
            if (max == null) return 0;
            else return max;
        }

        public int getNumberOfBuildings(BuildingType buildingType)
        {
            return buildingQuantities[buildingType];
        }

        public int getTotalBuildingTime()
        {
            return totalBuildingTime;
        }

        public int getTotalResource(BuildingResource resource)
        {
            return buildingResources[resource];
        }

        public void importData(String text)
        {
            var version = char.GetNumericValue(text[0]);

            resetGridData();

            String data = text.Substring(1);

            if (data == null) return;
            String[] dataStrings = data.Split(Converter.seperator);

            if (dataStrings.Length > 0 && dataStrings[0] != null) importSingleTiles(BuildingType.WOODEN_WALL, dataStrings[0]);
            if (dataStrings.Length > 1 && dataStrings[1] != null) importSingleTiles(BuildingType.STONE_WALL, dataStrings[1]);

            if (dataStrings.Length > 2 && dataStrings[2] != null)
            {
                int i = 0;
                while (i < dataStrings[2].Length)
                {
                    int ordinal = (int)Converter.alphaNumericToInt(dataStrings[2][i]);
                    uint x = Converter.alphaNumericToInt(dataStrings[2][i + 1]);
                    uint y = Converter.alphaNumericToInt(dataStrings[2][i + 2]);

                    BuildingType buildingType = BuildingType.Values[ordinal];
                    int id = getNewId();

                    for (uint k = x; k < x + buildingType.Dimension.Width; k++)
                    {
                        for (uint l = y; l < y + buildingType.Dimension.Height; l++)
                        {
                            gridData[k][l] = new TileBuilding(buildingType, id);
                        }
                    }

                    i += 3;
                }
            }
            if (dataStrings.Length > 3 && dataStrings[3] != null) importSingleTiles(BuildingType.MOAT, dataStrings[3]);
            if (dataStrings.Length > 4 && dataStrings[4] != null) importSingleTiles(BuildingType.KILLING_PIT, dataStrings[4]);
            updateDesignStats();

            if (version > 2) throw new ArgumentOutOfRangeException(version.ToString());
        }

        public void removeBuilding(TileBuilding building)
        {
            if (building == null) throw new NoNullAllowedException();

            int id = building.getBuildingId();

            for (int i = 0; i < CASTLE_BOUNDRY_LENGTH; i++)
            {
                for (int j = 0; j < CASTLE_BOUNDRY_LENGTH; j++)
                {
                    if (gridData[i][j] != null &&
                        gridData[i][j].getBuildingId() == id)
                    {
                        gridData[i][j] = null;
                    }
                }
            }
            updateDesignStats();
        }

        public void resetGridData()
        {
            for (int i = 0; i < gridData.Length; i++)
            {
                for (int j = 0; j < gridData[i].Length; j++)
                {
                    gridData[i][j] = null;
                }
            }

            for (int i = 22; i < 22 + BuildingType.KEEP.Dimension.Width; i++)
            {
                for (int j = 22; j < 22 + BuildingType.KEEP.Dimension.Height; j++)
                {
                    gridData[i][j] = new TileBuilding(BuildingType.KEEP, 0);
                }
            }
            updateDesignStats();
        }

        private int calculateNumberOfBuildings(BuildingType buildingType, int numberOfTiles)
        {
            return numberOfTiles / (buildingType.Dimension.Width * buildingType.Dimension.Height);
        }

        private int getNewId()
        {
            return ++lastIdUsed;
        }

        private void importSingleTiles(BuildingType buildingType, String dataString)
        {
            int i = 0;
            while (i < dataString.Length)
            {
                uint x = Converter.alphaNumericToInt(dataString[i]);
                uint y = Converter.alphaNumericToInt(dataString[i + 1]);

                gridData[x][y] = new TileBuilding(buildingType, getNewId());

                i += 2;
            }
        }

        private void setMaxBuildings()
        {
            maxBuildings.Add(BuildingType.MOAT, 500);
            maxBuildings.Add(BuildingType.BALLISTA_TOWER, 10);
            maxBuildings.Add(BuildingType.TURRET, 10);
            maxBuildings.Add(BuildingType.GUARD_HOUSE, 38);
        }

        private void updateDesignStats()
        {
            designErrors.Clear();
            totalBuildingTime = 0;

            foreach (BuildingResource buildingResource in Enum.GetValues(typeof(BuildingResource)))
            {
                buildingResources[buildingResource] = 0;
            }

            int[] buildingCounts = new int[BuildingType.Values.Count];

            for (int i = 0; i < gridData.Length; i++)
            {
                for (int j = 0; j < gridData[i].Length; j++)
                {
                    TileBuilding building = gridData[i][j];
                    if (building != null) buildingCounts[building.getBuildingType().Order]++;
                }
            }

            foreach (BuildingType buildingType in BuildingType.Values)
            {
                int numberOfBuildings = calculateNumberOfBuildings(buildingType, buildingCounts[buildingType.Order]);
                buildingQuantities[buildingType] = numberOfBuildings;

                foreach (BuildingResource buildingResource in Enum.GetValues(typeof(BuildingResource)))
                {
                    int cumulativeCost = buildingResources[buildingResource] + buildingType.getCost(buildingResource) * numberOfBuildings;

                    buildingResources[buildingResource] = cumulativeCost;
                }
                totalBuildingTime += buildingType.BuildTime * numberOfBuildings;
            }

            foreach (BuildingType buildingType in maxBuildings.Keys)
            {
                String designError = validateNumberOfBuildings(
                    buildingType,
                    buildingCounts[buildingType.Order],
                    maxBuildings[buildingType]);

                if (designError != null) designErrors.Add(designError);
            }
        }

        private String validateNumberOfBuildings(BuildingType buildingType, int numberOfTiles, int maxNumberOfBuildings)
        {
            int numberOfBuildings = calculateNumberOfBuildings(buildingType, numberOfTiles);

            if (numberOfBuildings > maxNumberOfBuildings)
            {
                return "Error: " + numberOfBuildings + " " + buildingType + "s (" + maxNumberOfBuildings + " max)";
            }
            else return null;
        }
    }
}