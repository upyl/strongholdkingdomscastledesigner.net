﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using Dimension = System.Drawing.Size;

namespace StrongholdKingdomsCastleDesigner.NET.src
{
    public class BuildingType : IEquatable<BuildingType>
    {
        public static BuildingType BALLISTA_TOWER;
        public static BuildingType GREAT_TOWER;
        public static BuildingType GUARD_HOUSE;
        public static BuildingType KEEP;
        public static BuildingType KILLING_PIT;
        public static BuildingType LARGE_TOWER;
        public static BuildingType LOOKOUT_TOWER;
        public static BuildingType MOAT;
        public static BuildingType SMALL_TOWER;
        public static BuildingType SMELTER;
        public static BuildingType STONE_GATEHOUSE;
        public static BuildingType STONE_WALL;
        public static BuildingType TURRET;
        public static BuildingType WOODEN_GATEHOUSE;
        public static BuildingType WOODEN_TOWER;
        public static BuildingType WOODEN_WALL;
        private static object sync;
        private static IList<BuildingType> types = new List<BuildingType>();
        private IDictionary<BuildingResource, int> buildingResources;

        static BuildingType()
        {
            sync = new object();
            BALLISTA_TOWER = new BuildingType("BALLISTA_TOWER", Color.FromArgb(230, 200, 60), new Dimension(3, 3), false, new int[] { 0, 10000, 0, 0 }, 18000);
            GREAT_TOWER = new BuildingType("GREAT_TOWER", Color.FromArgb(200, 200, 200), new Dimension(5, 5), true, new int[] { 2500, 0, 0, 0 }, 86400);
            GUARD_HOUSE = new BuildingType("GUARD_HOUSE", Color.FromArgb(255, 200, 180), new Dimension(3, 3), false, new int[] { 0, 400, 0, 0 }, 10800);
            KEEP = new BuildingType("KEEP", Color.FromArgb(0, 0, 0), new Dimension(8, 8), false, new int[] { 0, 0, 0, 0 }, 0);
            KILLING_PIT = new BuildingType("KILLING_PIT", Color.FromArgb(120, 100, 0), new Dimension(1, 1), false, new int[] { 0, 0, 100, 0 }, 3600);
            LARGE_TOWER = new BuildingType("LARGE_TOWER", Color.FromArgb(200, 200, 200), new Dimension(4, 4), true, new int[] { 1500, 0, 0, 0 }, 57600);
            LOOKOUT_TOWER = new BuildingType("LOOKOUT_TOWER", Color.FromArgb(200, 200, 200), new Dimension(2, 2), true, new int[] { 300, 0, 0, 0 }, 14400);
            MOAT = new BuildingType("MOAT", Color.FromArgb(0, 200, 255), new Dimension(1, 1), false, new int[] { 0, 0, 0, 20 }, 900);
            SMALL_TOWER = new BuildingType("SMALL_TOWER", Color.FromArgb(200, 200, 200), new Dimension(3, 3), true, new int[] { 800, 0, 0, 0 }, 28800);
            SMELTER = new BuildingType("SMELTER", Color.FromArgb(200, 30, 30), new Dimension(4, 4), false, new int[] { 0, 0, 400, 0 }, 21600);
            STONE_GATEHOUSE = new BuildingType("STONE_GATEHOUSE", Color.FromArgb(100, 100, 100), new Dimension(3, 3), true, new int[] { 500, 0, 0, 0 }, 7200);
            STONE_WALL = new BuildingType("STONE_WALL", Color.FromArgb(230, 230, 230), new Dimension(1, 1), false, new int[] { 100, 0, 0, 0 }, 900);
            TURRET = new BuildingType("TURRET", Color.FromArgb(0, 0, 80), new Dimension(2, 2), false, new int[] { 2000, 0, 0, 0 }, 14400);
            WOODEN_GATEHOUSE = new BuildingType("WOODEN_GATEHOUSE", Color.FromArgb(100, 50, 0), new Dimension(3, 3), true, new int[] { 0, 200, 0, 0 }, 3600);
            WOODEN_TOWER = new BuildingType("WOODEN_TOWER", Color.FromArgb(125, 58, 0), new Dimension(2, 2), false, new int[] { 0, 200, 0, 0 }, 10800);
            WOODEN_WALL = new BuildingType("WOODEN_WALL", Color.FromArgb(150, 75, 0), new Dimension(1, 1), false, new int[] { 0, 20, 0, 0 }, 225);
        }

        private BuildingType(string name, Color color, Dimension dimension, bool gapRequired, int[] resourceCosts, int buildTime)
        {
            lock (sync)
            {
                Order = (uint)types.Count;
                types.Add(this);
            }
            this.name = name;
            this.Color = color;
            this.Dimension = dimension;
            this.GapRequired = gapRequired;
            this.BuildTime = buildTime;

            String urlPath = "Resources/" + name.ToLower() + ".png";
            if (urlPath != null)
            {
                ﻿Image = new Bitmap(urlPath);
                ﻿ValidOverlay = createOverlay(true);
                ﻿InvalidOverlay = createOverlay(false);
            }

            buildingResources = new Dictionary<BuildingResource, int>();
            ﻿buildingResources.Add(BuildingResource.STONE, resourceCosts[0]);
            ﻿buildingResources.Add(BuildingResource.WOOD, resourceCosts[1]);
            ﻿buildingResources.Add(BuildingResource.IRON, resourceCosts[2]);
            ﻿buildingResources.Add(BuildingResource.GOLD, resourceCosts[3]);
        }

        public static IList<BuildingType> Values
        {
            get
            {
                return types;
            }
        }

        public int BuildTime { get; private set; }

        public Color Color { get; private set; }

        public Dimension Dimension { get; private set; }

        public bool GapRequired { get; private set; }

        public Bitmap Image { get; private set; }

        public Bitmap InvalidOverlay { get; private set; }

        public uint Order
        {
            get;
            private set;
        }

        public Bitmap ValidOverlay { get; private set; }

        private string name { get; set; }

        public bool Equals(BuildingType other)
        {
            if (Order == other.Order) return true;
            return false;
        }

        public int getCost(BuildingResource buildingResource)
        ﻿{
        ﻿  ﻿  return buildingResources[buildingResource];
        ﻿}

        public override int GetHashCode()
        {
            return (int)Order;
        }

        public int[] getHotspot()
        {
            return new int[] { (Dimension.Width - 1) / 2, (Dimension.Height - 1) / 2 };
        }

        public override string ToString()
        {
            var words = name.ToLower().Split('_');

            StringBuilder nameT = new StringBuilder();
            foreach (var word in words)
            {
                if (nameT.Length > 0) nameT.Append(' ');
                nameT.Append(word.Substring(0, 1).ToUpper());
                if (word.Length > 1) nameT.Append(word.Substring(1));
            }

            return nameT.ToString();
        }

        private Bitmap createOverlay(bool valid)
        {
            Bitmap overlay = new Bitmap(Image.Width, Image.Height);
            Graphics graphics = Graphics.FromImage(overlay);
            ColorMatrix colormatrix = new ColorMatrix();
            if (!valid)
            {
                colormatrix.Matrix40 = 0.6F;
            }
            colormatrix.Matrix33 = 0.6F;
            ImageAttributes imgAttribute = new ImageAttributes();
            imgAttribute.SetColorMatrix(colormatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            graphics.DrawImage(Image, new Rectangle(0, 0, overlay.Width, overlay.Height), 0, 0, Image.Width, Image.Height, GraphicsUnit.Pixel, imgAttribute);
            graphics.Dispose();   // Releasing all resource used by graphics
            return overlay;
        }
    }
}